<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Models\Request;

class Comment extends Model
{
    use HasFactory;
    public function createComment(Request $request){
        $this->texto= $request->texto;
        $this->user_id= $request->user_id;
        $this->book_id= $request->book_id;
        $this->save();
    }
    public function updateComment(Request $request){
        if($request->texto){
        $this->texto = $request->texto;
        }
        if($request->user_id){
        $this->user_id = $request->user_id;
        }
        if($request->book_id){
        $this->book_id = $request->book_id;
        }
        $comment->save();
        
    }
}




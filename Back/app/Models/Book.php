<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\http\Models\Request;


class Book extends Model
{
    public function createBook(Request $request){

        $book = new Book;
        $book->name = $request->name;            
        $book->category = $request->category;
        $book->debut = $request->debut;
        $book->price = $request->price;
        $book->resume = $request->resume;
        $book->condition = $request->condition;
        $book->save();
        }

    public function updateBook(Request $request, $id){
        if($request->name){
        $this->name = $request->name;
        }
        if($request->category){
        $this->category = $request->category;
        }
        if($request->debut){
        $this->debut = $request->debut;
        }
        if($request->price){
            $this->price = $request->price;
        }
        if($request->resume){
        $this->resume = $request->resume;
        }
        if($request->condition){
        $this->condition = $request->condition;
        }
        $book->save();
        
    }
}
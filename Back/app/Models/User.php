<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Models\Request;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    

    public function createUser(Request $request){
        $this->name= $request->name;
        $this->email= $request->email;
        $this->password= $request->password;
        $this->age= $request->age;
        $this->contact= $request->contact;
        $this->genre= $request->genre;
        $this->save();
    }
    public function updateUser(Request $request, $id){
        if($request->name){
            $this->name = $request->name;
        }
        if($request->email){
            $this->email = $request->email;
        }
        if($request->password){
            $this->password = $request->passrword;
        }
        if($request->age){
            $this->age = $request->age;
        }
        if($request->contact){
            $this->contact = $request->contact;
        }
        if($request->genre){
            $this->genre = $request->genre;
        }
        $this->save();
        }
}

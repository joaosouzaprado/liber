<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index() {
        $user = User::all();
        return response()->json(['user' =>$user],200);
    }
    public function createUser(Request $request, $id){
        $user = User::find($id);
        $user->createUser($request);
        return response()->json(['user' => $user],200);
    }
    public function updateUser(Request $request, $id){
        $user = User::find($id);
        $user->updateUser($request);
        return response()->json(['user' => $user],200);
    }
    public function delete($id){
        $user = User::find($id);
        $user->destroy();
        return response()->json(['Usuario deletado com sucesso!'],200);
    }
}    

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function index() {
        $comment = Comment::all();
        return response()->json(['comment' =>$comment],200);
    }
    public function createComment(Request $request, $id){
        $comment = Comment::find($id);
        $comment->createComment($request);
        return response()->json(['comment' => $comment],200);
    }
    public function updateComment(Request $request, $id){
        $comment = Comment::find($id);
        $comment->updateCommment($request);
        return response()->json(['comment' => $comment],200);
    }
    public function delete($id){
        $comment = Comment::find($id);
        $comment->destroy();
        return response()->json(['Comentario deletado com sucesso!'],200);
    }
}

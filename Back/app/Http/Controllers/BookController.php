<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    public function index() {
        $book = Book::all();
        return response()->json(['book' =>$book],200);
    }
    public function createBook(Request $request, $id){
        $book = Book::find($id);
        $book->createBook($request);
        return response()->json(['book' => $book],200);
    }
    public function updateBook(Request $request, $id){
        $book = Book::find($id);
        $book->updateBook($request);
        return response()->json(['book' => $book],200);
    }
    public function delete($id){
        $book = Book::find($id);
        $book->destroy();
        return response()->json(['Livro deletado com sucesso!'],200);
    }
}
